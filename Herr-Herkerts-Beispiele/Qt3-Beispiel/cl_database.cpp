/***************************************************************************
                          cl_database.cpp  -  description
                             -------------------
    begin                : Fre Mai 14 22:20:41 CEST 2004
    copyright            : (C) 2004 by 
    email                : 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cl_database.h"

#include <qsqlcursor.h>

//---------------------------------------------------------------------------
Cl_Database::Cl_Database(QWidget *parent, const char *name)
            : QWidget(parent, name)
{
     pO_Btn_Open = new QPushButton("open",this);
     pO_Btn_Open->setGeometry(10,10,50,20);
     connect( pO_Btn_Open,
              SIGNAL( clicked()) ,
              this,
              SLOT( slt_Open_clicked() ) );

     pO_Btn_Close = new QPushButton("close",this);
     pO_Btn_Close->setGeometry(70,10,50,20);
     connect( pO_Btn_Close,
              SIGNAL( clicked()) ,
              this,
              SLOT( slt_Close_clicked() ) );

     pO_Lbl_Host = new QLabel("Host",this);
     pO_Lbl_Host->setGeometry(10,30,150,20);
          pO_LEd_Host = new QLineEdit(this);
          pO_LEd_Host->setGeometry(160,30,235,20);
          pO_LEd_Host->setText("127.0.0.1");

     pO_Lbl_Port = new QLabel("Port",this);
     pO_Lbl_Port->setGeometry(10,55,150,20);
          pO_LEd_Port = new QLineEdit(this);
          pO_LEd_Port->setGeometry(160,55,235,20);
          pO_LEd_Port->setText("3306");

     pO_Lbl_Driver = new QLabel("Driver",this);
     pO_Lbl_Driver->setGeometry(10,80,150,20);
          pO_LEd_Driver = new QLineEdit(this);
          pO_LEd_Driver->setGeometry(160,80,235,20);
          pO_LEd_Driver->setText("QMYSQL3");

     pO_Lbl_DatabaseName = new QLabel("Datenbankname",this);
     pO_Lbl_DatabaseName->setGeometry(10,105,150,20);
          pO_LEd_DatabaseName = new QLineEdit(this);
          pO_LEd_DatabaseName->setGeometry(160,105,235,20);
          pO_LEd_DatabaseName->setText("eine_DB");

     pO_Lbl_User = new QLabel("Username",this);
     pO_Lbl_User->setGeometry(10,130,150,20);
          pO_LEd_User = new QLineEdit(this);
          pO_LEd_User->setGeometry(160,130,235,20);
          pO_LEd_User->setText("");

     pO_Lbl_Password = new QLabel("Password",this);
     pO_Lbl_Password->setGeometry(10,155,150,20);
          pO_LEd_Password = new QLineEdit(this);
          pO_LEd_Password->setGeometry(160,155,235,20);
          pO_LEd_Password->setText("");



     pO_SQL_Editor = new QMultiLineEdit(this);
     pO_SQL_Editor->setGeometry(400,10,400,200);
     pO_SQL_Editor->append("Select * From Artikel ");

     pO_Btn_ExecuteQuery = new QPushButton("ExecuteQuery",this);
     pO_Btn_ExecuteQuery->setGeometry(400,215,100,20);
     connect( pO_Btn_ExecuteQuery,
              SIGNAL( clicked()) ,
              this,
              SLOT( slt_ExecuteQuery_clicked() ) );

     pO_Tabelle_1 = new QTable(this);
     pO_Tabelle_1->setNumRows(100);
     pO_Tabelle_1->setNumCols(2);
     pO_Tabelle_1->setGeometry(400,240,400,200);
     pO_Tabelle_1->show();

                                                            
     pO_Meldungen = new QMultiLineEdit(this);
     pO_Meldungen->setGeometry(10,400,235,100);
                    
  /*   pO_Lbl_Driver = new QLabel("Datenbankname",this);
     pO_Lbl_Driver->setGeometry(10,30,150,20);
          pO_LEd_Driver = new QLineEdit(this);
          pO_LEd_Driver->setGeometry(160,30,235,20);
          pO_LEd_Driver->setText("60");*/

}
//---------------------------------------------------------------------------
Cl_Database::~Cl_Database()
{
}
//---------------------------------------------------------------------------
Cl_Database::slt_Open_clicked()
{
   int l_i =   Open_Database();  
}
//---------------------------------------------------------------------------
Cl_Database::slt_Close_clicked()
{
   Close_Database();
}
//---------------------------------------------------------------------------
void Cl_Database::Close_Database()
{
     pO_Database->close();
//     delete pO_Database;
}
//---------------------------------------------------------------------------
int Cl_Database::Open_Database()
{
        pO_Database =  QSqlDatabase::addDatabase( "QMYSQL3" );
//        pO_Database = new QSqlDatabase( "QMYSQL3" );

        if ( ! pO_Database )
        {
            qWarning( "Failed to connect to the database driver" );
            return 1;
        }
        pO_Database->setDatabaseName( pO_LEd_DatabaseName->text() );
        pO_Database->setUserName( pO_LEd_User->text() );
        pO_Database->setPassword( pO_LEd_Password->text() );
        pO_Database->setHostName( pO_LEd_Host->text() );
        pO_Database->setPort ( pO_LEd_Port->text().toInt() );

        if ( pO_Database->open() )
        {
             pO_Meldungen->append( "Datenbank ge�ffnet" );          
            // qWarning( "Datenbank erfolgreich ge�ffnet" );
            // Database successfully opened; we can now issue SQL commands.
        } else
        {
             pO_Meldungen->append( "Datenbank nicht ge�ffnet:"
                        + pO_Database->lastError().databaseText() );
                        
   /*          qWarning( "Datenbank nicht ge�ffnet:"
                        + pO_Database->lastError().databaseText() );*/
        }  
        return 0;
}
//---------------------------------------------------------------------------
Cl_Database::slt_ExecuteQuery_clicked()
{
   QSqlQuery L_query;
//   QSqlCursor L_query;
   QString l_Str;
   l_Str= pO_SQL_Editor->text();
   L_query.exec( l_Str );
   if ( L_query.isActive() )
   {
      int i = 0;
      int j;
      int s=0;
    QString L_s;
     L_query.first();
     QVariant L_V=L_query.value(s);
   s=3;
   pO_Tabelle_1->setNumCols(s+1);
      bool bl_Stay=L_query.first();
      while ( bl_Stay)
      {
        for (j=0; j<s ;j++)
        {
     //     L_s=L_query.value(j).toString();

          if (L_query.value(j).toString().isNull() )
          {
            pO_Tabelle_1->setText( i, j, "Null" );
          }
          else
          {
            pO_Tabelle_1->setText( i, j, L_query.value(j).toString() );
          }
       }
       i++;
       bl_Stay=L_query.next();
     }
   }
}
//---------------------------------------------------------------------------

