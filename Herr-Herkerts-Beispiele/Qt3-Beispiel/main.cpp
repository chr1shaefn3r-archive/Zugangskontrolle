/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Fre Mai 14 22:20:41 CEST 2004
    copyright            : (C) 2004 by 
    email                : 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>

#include "cl_database.h"

static const char *description =
	I18N_NOOP("Cl_Database");
// INSERT A DESCRIPTION FOR YOUR APPLICATION HERE
	
	
static KCmdLineOptions options[] =
{
  { 0, 0, 0 }
  // INSERT YOUR COMMANDLINE OPTIONS HERE
};

int main(int argc, char *argv[])
{

  KAboutData aboutData( "cl_database", I18N_NOOP("Cl_Database"),
    VERSION, description, KAboutData::License_GPL,
    "(c) 2004, ", 0, 0, "");
  aboutData.addAuthor("",0, "");
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication a;
  Cl_Database *cl_database = new Cl_Database();
  a.setMainWidget(cl_database);
  cl_database->show();  

  return a.exec();
}
