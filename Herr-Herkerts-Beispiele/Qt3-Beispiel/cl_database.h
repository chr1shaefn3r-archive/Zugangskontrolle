/***************************************************************************
                          cl_database.h  -  description
                             -------------------
    begin                : Fre Mai 14 22:20:41 CEST 2004
    copyright            : (C) 2004 by 
    email                : 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CL_DATABASE_H
#define CL_DATABASE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <kapp.h>
#include <qwidget.h>
#include <qlabel.h>

#include <qsqldatabase.h>
#include <qstring.h>

#include <qpushbutton.h>
#include <qmultilineedit.h>
#include <qlineedit.h>
#include <qtable.h>

//#include <qtextview.h>


/** Cl_Database is the base class of the project */
class Cl_Database : public QWidget
{
  Q_OBJECT 
  public:
    /** construtor */
    Cl_Database(QWidget* parent=0, const char *name=0);
    /** destructor */
    ~Cl_Database();

  private:
    QPushButton* pO_Btn_Open;
    QPushButton* pO_Btn_Close;
    
    QLabel*    pO_Lbl_Host;
    QLineEdit* pO_LEd_Host;

    QLabel*    pO_Lbl_Port;
    QLineEdit* pO_LEd_Port;

    QLabel*    pO_Lbl_Driver;
    QLineEdit* pO_LEd_Driver;
    
    QLabel*    pO_Lbl_DatabaseName;
    QLineEdit* pO_LEd_DatabaseName;

    QLabel*    pO_Lbl_User;
    QLineEdit* pO_LEd_User;

    QLabel*    pO_Lbl_Password;
    QLineEdit* pO_LEd_Password;

                                                                        
    QString s_Treibername;
    QSqlDatabase *pO_Database;

    QMultiLineEdit   *pO_SQL_Editor;
    QMultiLineEdit   *pO_Meldungen;

    QPushButton* pO_Btn_ExecuteQuery;
    QTable*      pO_Tabelle_1;
            
    int Open_Database();
    void Close_Database();
   private slots:     
     slt_Open_clicked();
     slt_Close_clicked();
     slt_ExecuteQuery_clicked();

    
};

#endif
