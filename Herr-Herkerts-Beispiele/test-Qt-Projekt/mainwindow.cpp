#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

        db = QSqlDatabase::addDatabase("QODBC");// "MySQL"
        model = new QSqlQueryModel();

        db.setHostName("localhost");
            db.setDatabaseName("dieDB");
            db.setPort(3306);

            db.setUserName("derUser");
            db.setPassword("dasPW");

           if (!db.open())
            {
                qDebug() << db.lastError().text();
                return;
            }
            model->setQuery("SELECT name, vorname FROM t_user", db);

            model->setHeaderData(0, Qt::Horizontal, QObject::tr("Name"));
            model->setHeaderData(1, Qt::Horizontal, QObject::tr("Vorname"));

            ui->tableView->setModel(model);

  }

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
