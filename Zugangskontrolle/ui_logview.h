#ifndef UI_LOGVIEW_H
#define UI_LOGVIEW_H

#include <QWidget>
#include "steuerung.h"
class Steuerung;
#include <QTableView>
#include <QCalendarWidget>

class UI_LogView : public QWidget
{
public:
    UI_LogView(QWidget *parent = 0, Steuerung *pSteuerung=0);
private:
    Steuerung *dieSteuerung;
public:
    QCalendarWidget *calendarWidget;
    QTableView *logTableView;
};

#endif // UI_LOGVIEW_H
