/* main.cpp */

#include <QtGui/QApplication>

#include "steuerung.h"

int main(int argc, char **argv)
{
        QApplication* programm        = new QApplication(argc, argv);
        Steuerung*  programmFenster = new Steuerung();

        programm->exec();

        delete programmFenster;
        delete programm;

        return 0;
}
