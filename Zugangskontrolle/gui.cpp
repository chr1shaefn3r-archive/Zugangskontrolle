#include "gui.h"

GUI::GUI(QWidget *parent, Steuerung *pSteuerung) : QWidget(parent)
{
    dieSteuerung = pSteuerung;
    b_Reload = new QPushButton("&Reload", this);
    //                     x   y    w   h
    b_Reload->setGeometry(20, 20, 140, 30);
    b_Reload->show();

    comboBoxTuer = new QComboBox(this);
    comboBoxTuer->setGeometry(170, 20, 350, 30);
    comboBoxTuer->setView(new QTableView());
    comboBoxTuer->setModelColumn(1);
    comboBoxTuer->show();

    comboBoxMitarbeiter = new QComboBox(this);
    comboBoxMitarbeiter->setGeometry(20, 60, 550, 30);
    comboBoxMitarbeiter->setView(new QTableView());
    comboBoxMitarbeiter->setModelColumn(1);
    comboBoxMitarbeiter->show();

    lb_Result = new QLabel("Ergebnis:",this);
    lb_Result->setGeometry(20, 100, 140, 30);
    lb_Result->show();

    le_Result = new QLineEdit(this);
    le_Result->setGeometry(80, 100, 180, 30);
    le_Result->show();

    b_Test = new QPushButton("&Test", this);
    b_Test->setGeometry(270, 100, 140, 30);
    b_Test->show();

    b_Read = new QPushButton("&Read", this);
    b_Read->setGeometry(420, 100, 140, 30);
    b_Read->show();

    lb_Picture = new QLabel("", this);
    QPixmap picture;
    picture.load("grundriss.jpg");
    lb_Picture->setGeometry(20, 140, picture.width(), picture.height());
    lb_Picture->setPixmap(picture);
    lb_Picture->show();

    QFont font("Arial", 12, QFont::Bold);
    QFont fontSmall("Arial", 10, QFont::Bold);

    lb_Raeume[0] = new QLabel("Buero\nD201", this);
    lb_Raeume[0]->setFont(font);
    lb_Raeume[0]->setGeometry(325, 146, 52, 98);
    lb_Raeume[0]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[0]->show();

    lb_Raeume[1] = new QLabel("Buero\nD202", this);
    lb_Raeume[1]->setFont(font);
    lb_Raeume[1]->setGeometry(392, 146, 67, 98);
    lb_Raeume[1]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[1]->show();

    lb_Raeume[2] = new QLabel("Buero\nD203", this);
    lb_Raeume[2]->setFont(font);
    lb_Raeume[2]->setGeometry(461, 146, 74, 98);
    lb_Raeume[2]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[2]->show();

    lb_Raeume[3] = new QLabel("Buero\nD204", this);
    lb_Raeume[3]->setFont(font);
    lb_Raeume[3]->setGeometry(537, 146, 76, 152);
    lb_Raeume[3]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[3]->show();

    lb_Raeume[4] = new QLabel("Tressor\nD205", this);
    lb_Raeume[4]->setFont(font);
    lb_Raeume[4]->setGeometry(537, 299, 76, 146);
    lb_Raeume[4]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[4]->show();

    lb_Raeume[5] = new QLabel("Buero\nD206", this);
    lb_Raeume[5]->setFont(font);
    lb_Raeume[5]->setGeometry(461, 352, 75, 92);
    lb_Raeume[5]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[5]->show();

    lb_Raeume[6] = new QLabel("Empfang\nD207", this);
    lb_Raeume[6]->setFont(font);
    lb_Raeume[6]->setGeometry(350, 352, 109, 101);
    lb_Raeume[6]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[6]->show();

    lb_Raeume[7] = new QLabel("Empfang\nD208", this);
    lb_Raeume[7]->setFont(font);
    lb_Raeume[7]->setGeometry(153, 352, 87, 102);
    lb_Raeume[7]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[7]->show();

    lb_Raeume[8] = new QLabel("Buero\nD209", this);
    lb_Raeume[8]->setFont(font);
    lb_Raeume[8]->setGeometry(27, 380, 124, 72);
    lb_Raeume[8]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[8]->show();

    lb_Raeume[9] = new QLabel("Buero\nD210", this);
    lb_Raeume[9]->setFont(font);
    lb_Raeume[9]->setGeometry(27, 300, 124, 78);
    lb_Raeume[9]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[9]->show();
    
    lb_Raeume[10] = new QLabel("Besprechung\nD211", this);
    lb_Raeume[10]->setFont(font);
    lb_Raeume[10]->setGeometry(27, 147, 124, 151);
    lb_Raeume[10]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[10]->show();

    lb_Raeume[11] = new QLabel("Buero\nD212", this);
    lb_Raeume[11]->setFont(font);
    lb_Raeume[11]->setGeometry(152, 146, 64, 69);
    lb_Raeume[11]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[11]->show();

    lb_Raeume[12] = new QLabel("WC\nD213", this);
    lb_Raeume[12]->setFont(font);
    lb_Raeume[12]->setGeometry(218, 147, 83, 68);
    lb_Raeume[12]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[12]->show();

    lb_Raeume[13] = new QLabel("WC\nD214", this);
    lb_Raeume[13]->setFont(font);
    lb_Raeume[13]->setGeometry(241, 217, 60, 81);
    lb_Raeume[13]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[13]->show();

    lb_Raeume[14] = new QLabel("Kueche\nD215", this);
    lb_Raeume[14]->setFont(fontSmall);
    lb_Raeume[14]->setGeometry(183, 235, 56, 62);
    lb_Raeume[14]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[14]->show();

    lb_Raeume[15] = new QLabel("Archiv\nD216", this);
    lb_Raeume[15]->setFont(fontSmall);
    lb_Raeume[15]->setGeometry(183, 300, 56, 49);
    lb_Raeume[15]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[15]->show();

    lb_Raeume[16] = new QLabel("Heizung\nD217", this);
    lb_Raeume[16]->setFont(fontSmall);
    lb_Raeume[16]->setGeometry(242, 300, 58, 50);
    lb_Raeume[16]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[16]->show();

    lb_Raeume[17] = new QLabel("Archiv\nD218", this);
    lb_Raeume[17]->setFont(fontSmall);
    lb_Raeume[17]->setGeometry(373, 273, 86, 45);
    lb_Raeume[17]->setStyleSheet(dieSteuerung->styleSheetStandard);
    lb_Raeume[17]->show();

    calendarWidget = new QCalendarWidget(this);
    calendarWidget->setGeometry(630, 20, 250, 250);
    calendarWidget->show();

    logTableView = new QTableView(this);
    logTableView->setGeometry(630, 280, 300, 200);
    logTableView->show();
}
