#include "serialio.h"

SerialIO::SerialIO()
{
	fileDescriptor = this->openPort();
	if(!this->initPort(fileDescriptor)) {
		qDebug() << "Failed to initPort()!!";
	}
}

SerialIO::~SerialIO()
{
	this->closePort(fileDescriptor);
}

int SerialIO::openPort() {
	int fd; /* File descriptor for the port */

	if ((fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY)) == -1)  {
		perror("open_port: Unable to open /dev/ttyUSB0");
	} else {
		fcntl(fd, F_SETFL, 0);
	}
	return fd;
}

bool SerialIO::initPort(int pFD) {
	if(tcgetattr(pFD, &term_attr) != 0)
		return false;
	term_attr.c_cflag = B9600 | CS8 | CREAD;
	term_attr.c_iflag = 0;
	term_attr.c_oflag = 0;
	term_attr.c_lflag = 0;
	cfsetispeed(&term_attr,B9600);
	cfsetospeed(&term_attr,B9600);

	if(tcsetattr(pFD, TCSAFLUSH, &term_attr) != 0)
		return false;

	return true;
}

QString SerialIO::readSerialInput() {
	QString returnValue;
	char buffer[5] = {-1, -1, -1, -1, -1};
	int num, offset = 0, bytes_expected = 5;
	do {
		num = read(fileDescriptor, buffer + offset, 1024);
		offset += num;
	} while (offset < bytes_expected);
	returnValue = buffer[0];
	returnValue += buffer[1];
	returnValue += buffer[2];
	returnValue += buffer[3];
	returnValue += buffer[4];
	returnValue = returnValue.toAscii().toHex();
	return returnValue;
}

void SerialIO::closePort(int pFD) {
	close(pFD);
}
