#ifndef UI_ZUGANGSSIMULATION_H
#define UI_ZUGANGSSIMULATION_H

#include <QWidget>
#include "steuerung.h"
class Steuerung;
#include <QPushButton>
#include <QComboBox>
#include <QTableView>
#include <QLineEdit>
#include <QLabel>
#include <QPixmap>
#include <QFont>
#include <QCalendarWidget>

class UI_ZugangsSimulation : public QWidget
{
public:
    UI_ZugangsSimulation(QWidget *parent = 0, Steuerung *pSteuerung=0);
private:
    Steuerung *dieSteuerung;
public:
    QPushButton *b_Reload;
    QPushButton *b_Test;
    QPushButton *b_Read;
    QComboBox *comboBoxMitarbeiter;
    QComboBox *comboBoxTuer;
    QLabel *lb_Result;
    QLineEdit *le_Result;
    QLabel *lb_Picture;
    QLabel *lb_Raeume[18];
};

#endif // UI_ZUGANGSSIMULATION_H
