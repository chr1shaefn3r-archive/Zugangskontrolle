#include "ui_logview.h"

UI_LogView::UI_LogView(QWidget *parent, Steuerung *pSteuerung) : QWidget(parent)
{
    dieSteuerung = pSteuerung;
    calendarWidget = new QCalendarWidget(this);
    calendarWidget->setGeometry(20, 100, 250, 250);
    calendarWidget->show();

    logTableView = new QTableView(this);
    logTableView->setGeometry(290, 20, 700, 400);
    logTableView->show();

}
