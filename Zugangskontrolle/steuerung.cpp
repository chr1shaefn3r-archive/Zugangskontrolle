/* steuerung.cpp */

#include "steuerung.h"

/* Konstruktor */
Steuerung::Steuerung() : QMainWindow()
{
    this->setWindowTitle("Zugangskontrolle");

    oldIndex = 0;

    styleSheetStandard = "background-color: transparent; color: black; padding-left: 1px";
    styleSheetAusgewaehlt = "background-color: transparent; color: blue; padding-left: 1px";
    styleSheetAccessGranted = "background-color: #00FF30; color: black; padding-left: 1px";
    styleSheetAccessDenied = "background-color: red; color: black; padding-left: 1px";

    sqlQueryMitarbeiter = "SELECT ID, name, vorname, position, rfid FROM t_mitarbeiter";
    sqlQueryTuer = "SELECT ID, raumnummer, raumart FROM t_raum";
	sqlQueryOrt = "SELECT name, plz FROM t_ort";

    db = QSqlDatabase::addDatabase("QMYSQL");
    modelMitarbeiter = new QSqlQueryModel();
    modelTuer = new QSqlQueryModel();
    modelLog = new QSqlQueryModel();
	modelOrt = new QSqlQueryModel();

    db.setHostName("localhost");
        db.setDatabaseName("zugangskontrolle");
        db.setPort(3306);
        db.setUserName("zugangskontrolle");
        db.setPassword("zugangskontrolle");

    if(!db.open()) {
        qDebug() << db.lastError().text();
        return;
    }

    modelMitarbeiter->setQuery(sqlQueryMitarbeiter, db);
    modelTuer->setQuery(sqlQueryTuer, db);
	modelOrt->setQuery(sqlQueryOrt, db);

    modelMitarbeiter->setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
    modelMitarbeiter->setHeaderData(1, Qt::Horizontal, QObject::tr("Name"));
    modelMitarbeiter->setHeaderData(2, Qt::Horizontal, QObject::tr("Vorname"));
    modelMitarbeiter->setHeaderData(3, Qt::Horizontal, QObject::tr("Position"));
    modelMitarbeiter->setHeaderData(4, Qt::Horizontal, QObject::tr("RFID"));
    modelTuer->setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
    modelTuer->setHeaderData(1, Qt::Horizontal, QObject::tr("Raumnr."));
    modelTuer->setHeaderData(2, Qt::Horizontal, QObject::tr("Raumnart"));
	modelOrt->setHeaderData(0, Qt::Horizontal, QObject::tr("Name"));
	modelOrt->setHeaderData(1, Qt::Horizontal, QObject::tr("PLZ"));

	dieSerieleSchnittstelle = new SerialIO();

    // Menue Anlegen
    //-------------------------------------------------------
	/*menuDatei = this->menuBar()->addMenu("&Datei");
    // Untermenuepunkt "Beenden"
    //------------------------
        umBeenden = new QAction("&Beenden", menuDatei);
        umBeenden->setStatusTip("Zugangskontrolle Beenden");
	menuDatei->addAction(umBeenden);*/

    //----------------------
    // Tabulatorbereich
    //----------------------
    tabulatorRahmen = new QTabWidget();

    // Tabulatorenbereich zuweisen
    //-----------------------------
    grundContainer = new QWidget();
    layoutgrundContainer  = new QHBoxLayout();
    grundContainer->setLayout(layoutgrundContainer);
    this->setCentralWidget(grundContainer);
    layoutgrundContainer->addWidget(tabulatorRahmen);

	tabulator[0]  =  ui_ZugangsSimulation = new UI_ZugangsSimulation(tabulatorRahmen, this); // neuer Tabulator
    tabulator[1]  =  ui_LogView = new UI_LogView(tabulatorRahmen, this); // neuer Tabulator
	tabulator[2]  =  ui_Einlernen = new UI_Einlernen(tabulatorRahmen, this); // neuer Tabulator
	tabulatorRahmen->addTab(tabulator[0], "Simulation - Zugangskontrolle"); // Reitertext
	tabulatorRahmen->addTab(tabulator[1], "Logviewer"); // Reitertext
	tabulatorRahmen->addTab(tabulator[2], "Einlernen"); // Reitertext

    connect(ui_ZugangsSimulation->b_Reload, SIGNAL(clicked()), SLOT(b_ReloadPressed()));
    connect(ui_ZugangsSimulation->b_Test, SIGNAL(clicked()), SLOT(b_TestPressed()));
	connect(ui_ZugangsSimulation->b_Read, SIGNAL(clicked()), SLOT(b_ReadZugangssimulationPressed()));
    connect(ui_ZugangsSimulation->comboBoxTuer, SIGNAL(currentIndexChanged(int)), SLOT(comboBoxTuer_IndexChanged(int)));
    connect(ui_ZugangsSimulation->comboBoxMitarbeiter, SIGNAL(currentIndexChanged(int)), SLOT(comboBoxMitarbeiter_IndexChanged()));
    connect(ui_LogView->calendarWidget, SIGNAL(clicked(QDate)), SLOT(calendarWidget_Clicked(QDate)));
	connect(ui_Einlernen->b_Read, SIGNAL(clicked()), SLOT(b_ReadEinlernenPressed()));
	connect(ui_Einlernen->b_Speichern, SIGNAL(clicked()), SLOT(b_SpeichernEinlernenPressed()));

	ui_ZugangsSimulation->comboBoxMitarbeiter->setModel(modelMitarbeiter);
	ui_ZugangsSimulation->comboBoxTuer->setModel(modelTuer);
	ui_Einlernen->comboBoxOrt->setModel(modelOrt);

    // Statusleiste fuettern
    //-----------------------
    this->statusBar()->showMessage("Willkommen in der muendlichen Pruefung!");
    this->resize(1024,600);
    this->show();
}

/* Destruktor */
Steuerung::~Steuerung ()
{
	dieSerieleSchnittstelle->~SerialIO();
}

void Steuerung::b_ReadZugangssimulationPressed()
{
	QString serialPortInput = dieSerieleSchnittstelle->readSerialInput();
	qDebug() << serialPortInput;
	ui_ZugangsSimulation->comboBoxMitarbeiter->setModelColumn(4);
	int newIndex = ui_ZugangsSimulation->comboBoxMitarbeiter->findText(serialPortInput, Qt::MatchContains);
	ui_ZugangsSimulation->comboBoxMitarbeiter->setModelColumn(1);
	qDebug() << newIndex;
	if(newIndex >= 0)
		ui_ZugangsSimulation->comboBoxMitarbeiter->setCurrentIndex(newIndex);
	this->statusBar()->showMessage(ui_ZugangsSimulation->comboBoxMitarbeiter->currentText()+" Versucht den Raum zu betreten.");
	this->b_TestPressed();
}

void Steuerung::b_ReadEinlernenPressed()
{
	ui_Einlernen->le_RFID->setText(dieSerieleSchnittstelle->readSerialInput());
}

void Steuerung::b_ReloadPressed()
{
	modelMitarbeiter->setQuery(sqlQueryMitarbeiter, db);
	ui_ZugangsSimulation->comboBoxMitarbeiter->setModel(modelMitarbeiter);
	modelTuer->setQuery(sqlQueryTuer, db);
	ui_ZugangsSimulation->comboBoxTuer->setModel(modelTuer);
	ui_ZugangsSimulation->le_Result->setText("Reloaded!");
}

void Steuerung::b_SpeichernEinlernenPressed()
{
	sqlQueryAddMitarbeiter = "INSERT INTO `zugangskontrolle`.`t_mitarbeiter` ";
	sqlQueryAddMitarbeiter += "(`ID`, `name` , `vorname`, `rfid`, `position`, `fk_ort`) ";
	sqlQueryAddMitarbeiter += "VALUES (NULL , '"+ui_Einlernen->le_Name->text()+"', '"+ui_Einlernen->le_Vorname->text()+"', '"+ui_Einlernen->le_RFID->text()+"', '"+ui_Einlernen->le_Position->text()+"', '"+QString::number(ui_Einlernen->comboBoxOrt->currentIndex()+1)+"');";
	qDebug() << sqlQueryAddMitarbeiter;
	QSqlQuery sqlQuery(db);
	if(!sqlQuery.exec(sqlQueryAddMitarbeiter)) {
		this->statusBar()->showMessage(sqlQuery.lastError().text());
	} else {
		this->statusBar()->showMessage("Willkommen in der muendlichen Pruefung!");
		ui_Einlernen->le_Name->clear();
		ui_Einlernen->le_Vorname->clear();
		ui_Einlernen->le_Position->clear();
		ui_Einlernen->comboBoxOrt->setCurrentIndex(0);
		ui_Einlernen->le_RFID->clear();
	}

}

void Steuerung::b_TestPressed()
{
	/*************** Daten auslesen *****************/
	QSqlRecord sqlrecordMitarbeiter = modelMitarbeiter->record(ui_ZugangsSimulation->comboBoxMitarbeiter->currentIndex());
	QSqlField sqlfieldMitarbeiter = sqlrecordMitarbeiter.field("ID");
	QSqlRecord sqlrecordRaum = modelTuer->record(ui_ZugangsSimulation->comboBoxTuer->currentIndex());
	QSqlField sqlfieldRaum = sqlrecordRaum.field("ID");
	/*************** Abfrage *****************/
	ui_ZugangsSimulation->le_Result->setText(QString::number(oldIndex));
	QSqlQueryModel *modelTest = new QSqlQueryModel();
	QString sqlQueryTest = "";
	sqlQueryTest += "SELECT * ";
	sqlQueryTest += "FROM t_raum ";
	sqlQueryTest += "INNER JOIN t_GR ON t_raum.ID = t_GR.fk_raum ";
	sqlQueryTest += "INNER JOIN t_gruppe ON t_gruppe.ID = t_GR.fk_gruppe ";
	sqlQueryTest += "INNER JOIN t_MG ON t_gruppe.ID = t_MG.fk_gruppe ";
	sqlQueryTest += "INNER JOIN t_mitarbeiter ON t_mitarbeiter.ID = t_MG.fk_mitarbeiter ";
	sqlQueryTest += "WHERE t_mitarbeiter.ID = "+sqlfieldMitarbeiter.value().toString()+" ";
	sqlQueryTest += "AND t_raum.ID = "+sqlfieldRaum.value().toString();
	// qDebug() << sqlQueryTest;
	modelTest->setQuery(sqlQueryTest, db);
	if(modelTest->rowCount() > 0) {
		ui_ZugangsSimulation->le_Result->setText("Access Granted");
		ui_ZugangsSimulation->lb_Raeume[oldIndex]->setStyleSheet(this->styleSheetAccessGranted);
	} else {
		ui_ZugangsSimulation->le_Result->setText("Access Denied");
		ui_ZugangsSimulation->lb_Raeume[oldIndex]->setStyleSheet(this->styleSheetAccessDenied);
	}
	/*************** Log *****************/
	QDateTime* qdatetime = new QDateTime(QDateTime::currentDateTime());
	QTime qTime = qdatetime->time();
	QString uhrzeit = qTime.toString("hh:mm:ss");
	QDate qDate = qdatetime->date();
	QString datum = qDate.toString("yyyy-MM-dd");
	sqlQueryAddLog = "INSERT INTO `zugangskontrolle`.`t_log` ";
	sqlQueryAddLog += "(`ID`, `date` , `time`, `fk_mitarbeiter` , `fk_raum` ) ";
	sqlQueryAddLog += "VALUES (NULL , '"+datum+"', '"+uhrzeit+"', '"+sqlfieldMitarbeiter.value().toString()+"', '"+sqlfieldRaum.value().toString()+"');";
	qDebug() << sqlQueryAddLog;
	QSqlQuery sqlQuery(db);
	if(!sqlQuery.exec(sqlQueryAddLog)) {
		this->statusBar()->showMessage(sqlQuery.lastError().text());
	}
	/*************** LogTableView-Reload *****************/
	modelLog->setQuery(sqlQueryLog);
}

void Steuerung::comboBoxTuer_IndexChanged(int newIndex) {
	/*********************************************************************************/
	ui_ZugangsSimulation->lb_Raeume[oldIndex]->setStyleSheet(this->styleSheetStandard);
	/*********************************************************************************/
	ui_ZugangsSimulation->lb_Raeume[newIndex]->setStyleSheet(this->styleSheetAusgewaehlt);
	/*********************************************************************************/
	oldIndex = newIndex;
	ui_ZugangsSimulation->le_Result->setText("");
}

void Steuerung::comboBoxMitarbeiter_IndexChanged() {
	ui_ZugangsSimulation->lb_Raeume[oldIndex]->setStyleSheet(this->styleSheetAusgewaehlt);
}

void Steuerung::calendarWidget_Clicked(QDate clickedDate) {
	QString datum = clickedDate.toString("yyyy-MM-dd");
	sqlQueryLog = "SELECT time, t_mitarbeiter.name, t_mitarbeiter.vorname, t_raum.raumnummer ";
	sqlQueryLog += "FROM t_log ";
	sqlQueryLog += "INNER JOIN t_mitarbeiter ON t_mitarbeiter.ID = t_log.fk_mitarbeiter ";
	sqlQueryLog += "INNER JOIN t_raum ON t_raum.ID = t_log.fk_raum ";
	sqlQueryLog += "WHERE t_log.date = '"+datum+"'";
	sqlQueryLog += "ORDER BY t_log.time ASC";
	qDebug() << sqlQueryLog;
	modelLog->setQuery(sqlQueryLog, db);

	modelLog->setHeaderData(0, Qt::Horizontal, QObject::tr("Uhrzeit"));
	modelLog->setHeaderData(1, Qt::Horizontal, QObject::tr("Name"));
	modelLog->setHeaderData(2, Qt::Horizontal, QObject::tr("Vorname"));
	modelLog->setHeaderData(3, Qt::Horizontal, QObject::tr("Raumnr."));

	ui_LogView->logTableView->setModel(modelLog);
}
