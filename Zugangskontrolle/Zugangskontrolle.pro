# -------------------------------------------------
# Project created by QtCreator 2011-06-01T16:56:47
# -------------------------------------------------
QT += sql
TARGET = Zugangskontrolle
TEMPLATE = app
SOURCES += main.cpp \
    steuerung.cpp \
    ui_zugangssimulation.cpp \
    ui_logview.cpp \
    serialio.cpp \
    ui_einlernen.cpp
HEADERS += steuerung.h \
    ui_zugangssimulation.h \
    ui_logview.h \
    serialio.h \
    ui_einlernen.h
