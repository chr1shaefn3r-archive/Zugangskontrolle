#include "ui_einlernen.h"

UI_Einlernen::UI_Einlernen(QWidget *parent, Steuerung *pSteuerung) : QWidget(parent)
{
	dieSteuerung = pSteuerung;

	lb_Name = new QLabel("Name:", this);
	lb_Name->setGeometry(20, 20, 100, 30);
	lb_Name->show();
	le_Name = new QLineEdit(this);
	le_Name->setGeometry(140, 20, 200, 30);
	le_Name->show();

	lb_Vorname = new QLabel("Vorname:", this);
	lb_Vorname->setGeometry(400, 20, 100, 30);
	lb_Vorname->show();
	le_Vorname = new QLineEdit(this);
	le_Vorname->setGeometry(520, 20, 200, 30);
	le_Vorname->show();

	lb_Position = new QLabel("Position:", this);
	lb_Position->setGeometry(20, 70, 100, 30);
	lb_Position->show();
	le_Position = new QLineEdit(this);
	le_Position->setGeometry(140, 70, 200, 30);
	le_Position->show();

	lb_Ort = new QLabel("Ort:", this);
	lb_Ort->setGeometry(400, 70, 100, 30);
	lb_Ort->show();
	comboBoxOrt = new QComboBox(this);
	comboBoxOrt->setGeometry(520, 70, 200, 30);
	comboBoxOrt->setView(new QTableView(this));
	comboBoxOrt->setModelColumn(0);
	comboBoxOrt->show();

	lb_RFID = new QLabel("RFID:", this);
	lb_RFID->setGeometry(20, 120, 100, 30);
	lb_RFID->show();
	le_RFID = new QLineEdit(this);
	le_RFID->setGeometry(140, 120, 200, 30);
	le_RFID->show();

	b_Speichern = new QPushButton("&Speichern", this);
	b_Speichern->setGeometry(400, 120, 100, 30);
	b_Speichern->show();

	b_Read = new QPushButton("&Read", this);
	b_Read->setGeometry(200, 160, 100, 30);
	b_Read->show();
}
