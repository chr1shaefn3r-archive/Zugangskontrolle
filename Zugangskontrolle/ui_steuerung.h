/********************************************************************************
** Form generated from reading UI file 'steuerung.ui'
**
** Created: Thu May 26 17:52:05 2011
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STEUERUNG_H
#define UI_STEUERUNG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Steuerung
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Steuerung)
    {
        if (Steuerung->objectName().isEmpty())
            Steuerung->setObjectName(QString::fromUtf8("Steuerung"));
        Steuerung->resize(600, 400);
        menuBar = new QMenuBar(Steuerung);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        Steuerung->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Steuerung);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Steuerung->addToolBar(mainToolBar);
        centralWidget = new QWidget(Steuerung);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        Steuerung->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(Steuerung);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Steuerung->setStatusBar(statusBar);

        retranslateUi(Steuerung);

        QMetaObject::connectSlotsByName(Steuerung);
    } // setupUi

    void retranslateUi(QMainWindow *Steuerung)
    {
        Steuerung->setWindowTitle(QApplication::translate("Steuerung", "Steuerung", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Steuerung: public Ui_Steuerung {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STEUERUNG_H
