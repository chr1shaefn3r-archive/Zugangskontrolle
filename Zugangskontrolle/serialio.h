#ifndef SERIALIO_H
#define SERIALIO_H

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <QDebug>
#include <QString>

class SerialIO
{
public:
    SerialIO();
	~SerialIO();
private:
	int fileDescriptor;
	char rfid[5];
	struct termios term_attr;
	int openPort();
	bool initPort(int);
	void closePort(int);
public:
	QString readSerialInput();
};

#endif // SERIALIO_H
