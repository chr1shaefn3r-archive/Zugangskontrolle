#ifndef UI_EINLERNEN_H
#define UI_EINLERNEN_H

#include <QWidget>
#include "steuerung.h"
class Steuerung;
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QTableView>

class UI_Einlernen : public QWidget
{
public:
	UI_Einlernen(QWidget *parent = 0, Steuerung *pSteuerung=0);
private:
	Steuerung* dieSteuerung;
public:
	QLabel* lb_Name;
	QLineEdit* le_Name;
	QLabel* lb_Vorname;
	QLineEdit* le_Vorname;
	QLabel* lb_Position;
	QLineEdit* le_Position;
	QLabel* lb_Ort;
	QComboBox* comboBoxOrt;
	QLabel* lb_RFID;
	QLineEdit* le_RFID;
	QPushButton* b_Speichern;
	QPushButton* b_Read;
};

#endif // UI_EINLERNEN_H
