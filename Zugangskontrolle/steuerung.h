/* steuerung.h */

#ifndef STEUERUNG_H
#define STEUERUNG_H

#include <QTabWidget>
#include <QWidget>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QMainWindow>
#include <QMenuBar>
#include <QStatusBar>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtSql/QSqlDriver>
#include <QDebug>
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QPalette>
#include <QColor>
#include "ui_zugangssimulation.h"
class UI_ZugangsSimulation;
#include "ui_logview.h"
class UI_LogView;
#include "ui_einlernen.h"
class UI_Einlernen;
#include "serialio.h"

class Steuerung : public QMainWindow
{
    Q_OBJECT
public:
    Steuerung(); //Konstruktor
   ~Steuerung(); //Destruktor
    QString styleSheetStandard;
    QString styleSheetAusgewaehlt;
    QString styleSheetAccessGranted;
    QString styleSheetAccessDenied;
private:
    QMenu* menuDatei;
      QAction* umBeenden;

    QWidget *grundContainer;
    QHBoxLayout *layoutgrundContainer;
    QTabWidget*  tabulatorRahmen;   //Reitertext
    QWidget* tabulator[3];   //da sind die QWidgets drin

    UI_LogView* ui_LogView;
    UI_ZugangsSimulation* ui_ZugangsSimulation;
	UI_Einlernen* ui_Einlernen;
	SerialIO *dieSerieleSchnittstelle;

    QSqlQueryModel *modelMitarbeiter;
    QSqlQueryModel *modelTuer;
    QSqlQueryModel *modelLog;
	QSqlQueryModel *modelOrt;
    QSqlDatabase db;
	QString sqlQueryMitarbeiter, sqlQueryTuer, sqlQueryOrt,
			sqlQueryLog, sqlQueryAddLog, sqlQueryAddMitarbeiter;
    bool first;
    int oldIndex;
private slots:
    void b_ReloadPressed();
    void b_TestPressed();
	void b_ReadZugangssimulationPressed();
	void b_ReadEinlernenPressed();
	void b_SpeichernEinlernenPressed();
    void comboBoxTuer_IndexChanged(int);
    void comboBoxMitarbeiter_IndexChanged();
    void calendarWidget_Clicked(QDate);
};

#endif
